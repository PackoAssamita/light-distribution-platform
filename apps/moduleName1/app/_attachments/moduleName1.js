/*
 Light Distribution Platform
 ===========================

 Copyright (C) 2005-2014 All rights reserved by
   Francisco José Moreno Llorca <packo@assamita.net>
   Francisco Jesús González Mata <chuspb@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

var sys = require('sys');

function create (options) {
	var module = options || {};
	
	module.message = function(){
		return 'moduleName1, message: loaded';
	}
	
	module.execute = function(request, response, conf, initial) {
		sys.puts(initial.message());
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.write('moduleName1, execute ok\n');
		sys.puts("moduleName1, execute ok\n");
		response.end();
	}
	
	return module;
}

exports.create = create;