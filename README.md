Light Distribution Platform
=========================
LDP is an application distribution platform where each node has it own nodejs (running apps) and it own couchdb instance. The apps are stored into the DB and they are replicated as the rest of the data through the replication system of couchdb. The apps use also couchdb as the persistent storage system. For horizontal scaling you have to add a node, thats all. The new node replicates against other node, it retrieves the data and the apps and nodejs automatically runs each application.

When a application code is updated, the change is replicated automatically by couchdb (as the rest of information) and notifies the change to nodejs. It restarts that application with the new version. So if you update the application code in a node of the net automatically all the instances of the application are updated.

The intend of this development is to have the posibility of deploying small applications based on nodejs easyly in a very dinamic way with just adding a node.

This software is under heavy development and it is in a conceptual and testing phase, it's just an experiment right now.

Authors
-------
* Francisco José Moreno Llorca | <packo@assamita.net> | https://github.com/kotejante
* Francisco Jesús González Mata | <chuspb@gmail.com> |  https://github.com/chuspb

Third parties licenses
----------------------
* Couchdb library: Copyright (C) 2007-2009 Christopher Lenz, All rights reserved.              
* httplib2 library: Copyright 2006, Joe Gregorio

License
-------

      Light Distribution Platform

     Copyright (C) 2005-2014 All rights reserved by
       Francisco José Moreno Llorca <packo@assamita.net>
       Francisco Jesús González Mata <chuspb@gmail.com>

     This program is free software; you can redistribute it and/or
     modify it under the terms of the GNU Lesser General Public License
     as published by the Free Software Foundation; either version 2
     of the License, or (at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU Lesser General Public License
     along with this program; if not, write to the Free Software
     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
